//
//  ListViewController.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class ListViewController: UIViewController {
  static let nibName = "ListViewController"
  
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var selectionButton: UIButton!
  
  var presenter: ListViewToPresenter?
  var optionListCell: [OptionListCell] = [OptionListCell(title: "Camara"),
                                          OptionListCell(title: "Foto"),
                                          OptionListCell(title: "Nombre Completo"),
                                          OptionListCell(title: "Número Telefonico"),
                                          OptionListCell(title: "Fecha de Nacimiento"),
                                          OptionListCell(title: "Sexo"),
                                          OptionListCell(title: "Color Favorito")]
  override func viewDidLoad() {
    super.viewDidLoad()
    getStyleView()
  }
  
  public static func initWithNibName() -> ListViewController {
    return ListViewController(nibName: nibName, bundle: Bundle(for: ListViewController.self))
  }
  
  func getStyleView() {
    tableView.delegate = self
    tableView.dataSource = self
    tableView.register(UINib(nibName: "OptionListTableViewCell", bundle: .local), forCellReuseIdentifier: "optionCell")
    selectionButton.layer.cornerRadius = 10
  }
  
  @IBAction func selectOptionList(_ sender: UIButton) {
    validateOption(listCell: optionListCell)
  }
  
  @objc func checkboxTapped(_ sender: UIButton) {
    let index = sender.tag
    optionListCell[index].isChecked.toggle()
    tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
  }
  
  func validateOption(listCell: [OptionListCell]) {
    let validateOption = listCell.contains { $0.isChecked }
    if validateOption {
      presenter?.goToSectionOptionList(optionMenu: listCell)
    } else {
      showAlert(title: "Aviso", message: "Es necesario seleccionar almenos una opcion.")
    }
  }
  
  func showAlert(title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: nil))
    self.present(alert, animated: true, completion: nil)
  }
}
extension ListViewController: UITableViewDataSource, UITableViewDelegate {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return optionListCell.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionListTableViewCell
    let option = optionListCell[indexPath.row]
    cell.getOptionCell(data: option)
    cell.checkButton.tag = indexPath.row
    cell.checkButton.addTarget(self, action: #selector(checkboxTapped(_:)), for: .touchUpInside)
    return cell
  }
}

extension ListViewController: ListPresenterToView {}
