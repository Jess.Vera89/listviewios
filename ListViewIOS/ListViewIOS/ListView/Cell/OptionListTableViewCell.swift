  //
  //  OptionListTableViewCell.swift
  //  ListViewIOS
  //
  //  Created by Jessica Vera Perez on 04/01/24.
  //

import UIKit

class OptionListTableViewCell: UITableViewCell {
  
  @IBOutlet weak var titleOption: UILabel!
  @IBOutlet weak var checkButton: UIButton!
  
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func getOptionCell(data: OptionListCell) {
    titleOption.text = data.title
    if data.isChecked {
      checkButton.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
    } else {
      checkButton.setImage(UIImage(systemName: "squareshape"), for: .normal)
    }
  }
  
}
