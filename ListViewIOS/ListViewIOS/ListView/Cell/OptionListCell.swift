  //
  //  OptionListCell.swift
  //  ListViewIOS
  //
  //  Created by Jessica Vera Perez on 04/01/24.
  //

import UIKit

class OptionListCell {
  var title: String
  var isChecked: Bool
  
  init(title: String, isChecked: Bool = false) {
    self.title = title
    self.isChecked = isChecked
  }
}

enum CellType {
    case camera
    case photo
    case fullName
    case phone
    case birthdate
    case gender
    case color
}

struct Section {
    var title: String
    var cells: [CellType]
    var selectedGenderIndex: Int?
}
