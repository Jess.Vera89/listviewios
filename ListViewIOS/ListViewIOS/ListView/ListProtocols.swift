//
//  ListProtocols.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

protocol ListPresenterToView {}
protocol ListViewToPresenter {
  func goToSectionOptionList(optionMenu: [OptionListCell])
}
protocol ListInteractorToPresenter{}
protocol ListRouterToPresenter {}
protocol ListPresenterToInteractor {}
protocol ListPresenterToRouter {
  func goToSectionOptionList(optionMenu: [OptionListCell], navigation: UINavigationController)
}
