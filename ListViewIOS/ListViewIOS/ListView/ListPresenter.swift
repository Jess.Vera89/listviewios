//
//  ListPresenter.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class ListPresenter {
  var view: ListPresenterToView?
  var interactor: ListPresenterToInteractor?
  var router: ListPresenterToRouter?
}

extension ListPresenter: ListViewToPresenter {
  func goToSectionOptionList(optionMenu: [OptionListCell]) {
    guard let navigationController = (view as? ListViewController)?.navigationController else { return }
    router?.goToSectionOptionList(optionMenu: optionMenu, navigation: navigationController)
  }
}
extension ListPresenter: ListInteractorToPresenter {}
extension ListPresenter: ListRouterToPresenter {}
