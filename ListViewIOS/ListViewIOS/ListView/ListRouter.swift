//
//  ListRouter.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class ListRouter {
  var presenter: ListRouterToPresenter?
  
  public func createModule(window: UIWindow?) -> UIViewController {
    let view = ListViewController.initWithNibName()
    let presenter = ListPresenter()
    let interactor = ListInteractor()
    let router = ListRouter()
    
    view.presenter = presenter
    presenter.view = view
    presenter.interactor = interactor
    presenter.router = router
    interactor.presenter = presenter
    
    let navigation = UINavigationController()
    navigation.viewControllers = [view]
    window?.rootViewController = navigation
    window?.makeKeyAndVisible()
    return view
  }
}

extension ListRouter: ListPresenterToRouter {
  func goToSectionOptionList(optionMenu: [OptionListCell], navigation: UINavigationController) {
    let vc = SectionOptionListRouter().createModule(optionMenu: optionMenu)
    navigation.pushViewController(vc, animated: true)
  }
}
