  //
  //  Extensions.swift
  //  ListViewIOS
  //
  //  Created by Jessica Vera Perez on 04/01/24.
  //

import UIKit

  // MARK: - Bundle
extension Bundle {
  static let local: Bundle = Bundle.init(for: ListViewController.self)
}

  // MARK: - UIImage

extension UIImageView {
  func load(url: URL) {
    DispatchQueue.global().async { [weak self] in
      if let data = try? Data(contentsOf: url) {
        if let image = UIImage(data: data) {
          DispatchQueue.main.async {
            self?.image = image
          }
        }
      } else {
        DispatchQueue.main.async {
          self?.image = UIImage(systemName: "photo.artframe")
        }
      }
    }
  }
}
