//
//  SectionOptionListPresenter.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class SectionOptionListPresenter {
  var view: SectionOptionListPresenterToView?
  var router: SectionOptionListPresenterToRouter?
  var interactor: SectionOptionListPresenterToInteractor?
}
extension SectionOptionListPresenter: SectionOptionListViewToPresenter {}
extension SectionOptionListPresenter: SectionOptionListInteractorToPresenter {}
extension SectionOptionListPresenter: SectionOptionListRouterToPresenter {}


