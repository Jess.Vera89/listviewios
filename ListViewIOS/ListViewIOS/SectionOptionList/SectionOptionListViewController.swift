  //
  //  SectionOptionListViewController.swift
  //  ListViewIOS
  //
  //  Created by Jessica Vera Perez on 04/01/24.
  //

import UIKit

class SectionOptionListViewController: UIViewController {
  
  static let nibName = "SectionOptionListViewController"
  @IBOutlet weak var tableViewSection: UITableView!
  
  var presenter: SectionOptionListViewToPresenter?
  var optionMenu: [OptionListCell] = []
  var sections: [Section] = []
  var optionGender: [OptionListCell] = [OptionListCell(title: "Mujer"), OptionListCell(title: "Hombre")]
  var optionColor = ["Negro", "Amarillo", "Rojo", "Azul", "Morado"]
  var indexSex = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    getStyleView()
  }
  
  public static func initWithNibName() -> SectionOptionListViewController {
    return SectionOptionListViewController(nibName: nibName, bundle: Bundle(for: SectionOptionListViewController.self))
  }
  
  func getStyleView() {
    tableViewSection.delegate = self
    tableViewSection.dataSource = self
    tableViewSection.register(UINib(nibName: "CameraCellTableViewCell", bundle: .local), forCellReuseIdentifier: "camera")
    tableViewSection.register(UINib(nibName: "ImageCellTableViewCell", bundle: .local), forCellReuseIdentifier: "photo")
    tableViewSection.register(UINib(nibName: "FullNameCellTableViewCell", bundle: .local), forCellReuseIdentifier: "fullName")
    tableViewSection.register(UINib(nibName: "BirthdateCellTableViewCell", bundle: .local), forCellReuseIdentifier: "birthdate")
    tableViewSection.register(UINib(nibName: "OptionListTableViewCell", bundle: .local), forCellReuseIdentifier: "optionCell")
    sections = convertToSections(optionListCell: optionMenu)
  }
  
  func convertToSections(optionListCell: [OptionListCell]) -> [Section] {
    var sections: [Section] = []
    
    for optionCell in optionListCell {
      switch optionCell.title {
      case "Camara":
        sections.append(Section(title: optionCell.title, cells: [.camera]))
      case "Foto":
        sections.append(Section(title: optionCell.title, cells: [.photo]))
      case "Nombre Completo":
        sections.append(Section(title: optionCell.title, cells: [.fullName]))
      case "Número Telefonico":
        sections.append(Section(title: optionCell.title, cells: [.phone]))
      case "Fecha de Nacimiento":
        sections.append(Section(title: optionCell.title, cells: [.birthdate]))
      case "Sexo":
        sections.append(Section(title: optionCell.title, cells: [.gender]))
      case "Color Favorito":
        sections.append(Section(title: optionCell.title, cells: [.color]))
      default:
        break
      }
    }
    
    return sections
  }
  
  @objc func checkboxTapped(_ sender: UIButton) {
    let index = sender.tag
    optionGender[index].isChecked.toggle()
    tableViewSection.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
  }
}

extension SectionOptionListViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return sections.count
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let sectionType = sections[section].cells
    switch sectionType {
    case [.gender]:
      return optionGender.count
    case [.color]:
      return optionColor.count
    default:
      return 1
    }
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellData = sections[indexPath.section].cells
    switch cellData {
    case [.camera]:
      let cell = tableView.dequeueReusableCell(withIdentifier: "camera", for: indexPath) as! CameraCellTableViewCell
      return cell
    case [.photo]:
      let cell = tableView.dequeueReusableCell(withIdentifier: "photo", for: indexPath) as! ImageCellTableViewCell
      cell.setURLString(in:"https://static.vecteezy.com/system/resources/previews/020/489/292/non_2x/3d-logo-of-apple-iphone-free-png.png")
      return cell
    case [.fullName]:
      let cell = tableView.dequeueReusableCell(withIdentifier: "fullName", for: indexPath) as! FullNameCellTableViewCell
      cell.getPlaceHolderIn("Nombre", isNumber: false)
      return cell
    case [.phone]:
      let cell = tableView.dequeueReusableCell(withIdentifier: "fullName", for: indexPath) as! FullNameCellTableViewCell
      cell.getPlaceHolderIn("Número Telefonico", isNumber: true)
      return cell
    case [.birthdate]:
      let cell = tableView.dequeueReusableCell(withIdentifier: "birthdate", for: indexPath) as! BirthdateCellTableViewCell
      return cell
    case [.gender]:
      let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionListTableViewCell
      cell.titleOption.text = optionGender[indexPath.row].title
      cell.checkButton.isSelected = optionGender[indexPath.row].isChecked
      cell.checkButton.addTarget(self, action: #selector(checkboxTapped(_:)), for: .touchUpInside)
      return cell
    default:
      let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! OptionListTableViewCell
      cell.titleOption.text = optionColor[indexPath.row]
      cell.checkButton.isHidden = true
      return cell
    }
  }
  
  func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return sections[section].title
  }
  
  func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    let sectionType = sections[indexPath.section].cells
    switch sectionType {
    case [.gender]:
      return UITableView.automaticDimension
    case [.color]:
      return UITableView.automaticDimension
    default:
      return 250
    }
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    let sectionType = sections[indexPath.section].cells
    switch sectionType {
    case [.camera], [.photo]:
      return 250
    default:
      return UITableView.automaticDimension
    }
  }
  
}



extension SectionOptionListViewController: SectionOptionListPresenterToView {}
