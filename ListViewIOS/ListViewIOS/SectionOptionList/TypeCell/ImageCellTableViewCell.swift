//
//  ImageCellTableViewCell.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class ImageCellTableViewCell: UITableViewCell {

  @IBOutlet weak var imageCell: UIImageView!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
  
  func setURLString(in imageURL: String) {
    imageCell.load(url: (URL(string: imageURL)!))
  }
    
}
