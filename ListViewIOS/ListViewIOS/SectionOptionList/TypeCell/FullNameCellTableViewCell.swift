  //
  //  FullNameCellTableViewCell.swift
  //  ListViewIOS
  //
  //  Created by Jessica Vera Perez on 04/01/24.
  //

import UIKit

class FullNameCellTableViewCell: UITableViewCell,UITextFieldDelegate {
  
  @IBOutlet weak var nameUser: UITextField!
  var numberActive = false
  override func awakeFromNib() {
    super.awakeFromNib()
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
  
  func getPlaceHolderIn(_ textfiel: String, isNumber: Bool ) {
    nameUser.placeholder = textfiel
    numberActive = isNumber
    nameUser.delegate = self
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if numberActive {
      let isNumeric = string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
      let currentText = textField.text ?? ""
      guard let stringRange = Range(range, in: currentText), isNumeric else {
        return false
      }
      
      let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
      return updatedText.count <= 10
    }
    
      let isNumeric = string.rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
      let currentText = textField.text ?? ""
      guard let stringRange = Range(range, in: currentText) else { return false }
      let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
      return updatedText.count <= 35
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}
