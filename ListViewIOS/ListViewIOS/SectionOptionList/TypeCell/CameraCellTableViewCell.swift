//
//  CameraCellTableViewCell.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class CameraCellTableViewCell: UITableViewCell, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var cameraButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
      cameraButton.layer.cornerRadius = 10
        cameraButton.addTarget(self, action: #selector(openCamera), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @objc func openCamera() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            UIApplication.shared.windows.first?.rootViewController?.present(imagePicker, animated: true, completion: nil)
        } else {
            print("Estas desde un simulador")
        }
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
        }
        picker.dismiss(animated: true, completion: nil)
    }

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}
