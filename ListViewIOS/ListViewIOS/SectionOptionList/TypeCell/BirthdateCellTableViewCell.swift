  //
  //  BirthdateCellTableViewCell.swift
  //  ListViewIOS
  //
  //  Created by Jessica Vera Perez on 04/01/24.
  //

import UIKit

class BirthdateCellTableViewCell: UITableViewCell, UITextFieldDelegate {
  
  @IBOutlet weak var birthdateTextField: UITextField!
  private var datePicker: UIDatePicker!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    datePicker = UIDatePicker()
    datePicker.datePickerMode = .date
    datePicker.locale = Locale(identifier: "es_ES")
    datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)

    birthdateTextField.inputView = datePicker
    birthdateTextField.delegate = self
  }
  
  @objc func datePickerValueChanged() {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy"
    birthdateTextField.text = dateFormatter.string(from: datePicker.date)
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    datePickerValueChanged() // Actualizar el texto del textField al valor actual del datePicker
    return true
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
  }
}
