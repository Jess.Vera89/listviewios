//
//  SectionOptionListRouter.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

class SectionOptionListRouter {
  var presenter: SectionOptionListRouterToPresenter?
  public func createModule(optionMenu: [OptionListCell]) -> UIViewController {
    let view = SectionOptionListViewController.initWithNibName()
    let presenter = SectionOptionListPresenter()
    let interactor = SectionOptionListInteractor()
    let router = SectionOptionListRouter()
    
    view.presenter = presenter
    view.optionMenu = optionMenu
    presenter.view = view
    presenter.interactor = interactor
    presenter.router = router
    router.presenter = presenter
    interactor.presenter = presenter
    return view
  }
}

extension SectionOptionListRouter: SectionOptionListPresenterToRouter {}
