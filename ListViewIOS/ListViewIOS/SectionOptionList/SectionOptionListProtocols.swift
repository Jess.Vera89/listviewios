//
//  SectionOptionListProtocols.swift
//  ListViewIOS
//
//  Created by Jessica Vera Perez on 04/01/24.
//

import UIKit

protocol SectionOptionListPresenterToView {}
protocol SectionOptionListViewToPresenter {}
protocol SectionOptionListInteractorToPresenter{}
protocol SectionOptionListRouterToPresenter {}
protocol SectionOptionListPresenterToInteractor {}
protocol SectionOptionListPresenterToRouter {}

